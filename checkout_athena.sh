#!/bin/bash

athena_version=24.0.18

# Use the parent directory of the currently executing script as the base directory
BASE_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)


setupATLAS

lsetup git

echo "USER:  "$USER
USER=berkeleylab
# Check if 'athena' directory exists, skip git atlas init-workdir if it does
if [ ! -d "athena" ]; then
    echo "Checking out athena"
    # git atlas init-workdir https://:@gitlab.cern.ch:8443/$USER/athena.git
    git atlas init-workdir ssh://git@gitlab.cern.ch:7999/toyamaza/CharmPhysics/athena.git
else
    echo "'athena' directory already exists. Skipping git atlas init-workdir."
fi

cd athena

# Check if the current branch is already the desired branch
current_branch=$(git rev-parse --abbrev-ref HEAD)
desired_branch="charmanalysis_athena_$athena_version"
if [ "$current_branch" != "$desired_branch" ]; then
    git checkout -b $desired_branch release/${athena_version}
else
    echo "Already on branch $desired_branch"
fi

echo "Checking for existing packages"
packages=("DerivationFrameworkBPhys" "CxxUtils")
existing_packages=$(git atlas listpkg)

for package in "${packages[@]}"; do
    if echo "$existing_packages" | grep -q "$package"; then
        echo "Package $package already exists. Skipping git atlas addpkg."
    else
        echo "Adding package $package"
        git atlas addpkg "$package"
    fi
done



# echo "Applying patches"
# git apply "$BASE_DIR/patches/athena_24.0.18.patch"


cd ..
# #!/bin/bash

# athena_version=24.0.18

# # Use the current working directory as the base directory
# BASE_DIR=$(pwd)


# # Function to apply a patch to a package
# apply_patch() {
#     local package=$1
#     local patchfile=$BASE_DIR/$2 # Use absolute path for the patchfile

#     # Check if the package directory exists
#     if [ ! -d "$package" ]; then
#         echo "Error: Package directory '$package' does not exist." >&2
#         return 1
#     fi

#     # Apply the patch
#     pushd "$package" > /dev/null
#     patch -p1 < "$patchfile"
#     popd > /dev/null
# }

# setupATLAS -c -el9

# lsetup git


# if [ ! -d "athena" ]; then
#     echo "Checking out athena"
#     git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git 
# else
#     echo "'athena' directory already exists. Skipping git atlas init-workdir."
# fi
# ls
# cd athena

# # Check if the current branch is already the desired branch
# current_branch=$(git rev-parse --abbrev-ref HEAD)
# desired_branch="charmanalysis_athena_$athena_version"
# if [ "$current_branch" != "$desired_branch" ]; then
#     git checkout -b $desired_branch release/${athena_version}
# else
#     echo "Already on branch $desired_branch"
# fi

# echo "Adding a few athena packages"
# git atlas addpkg DerivationFrameworkBPhys
# git atlas addpkg CxxUtils


# cd ..


# # echo "Applying patches"
# # # Apply patches to the packages
# # apply_patch athena "patches/athena_CascadeTools.patch" || exit 1
# # apply_patch athena "patches/athena_CxxUtils.patch" || exit 1
